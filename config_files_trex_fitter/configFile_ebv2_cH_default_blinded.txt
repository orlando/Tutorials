% -------------------------- %
% -------    JOB     ------- %
% -------------------------- %

Job: "base_ebv2_signal_cH_default_blinded"
   Label: "FCNC analysis"
   CmeLabel: "13 TeV"
   LumiLabel: "36.1 fb^{-1}"
   Lumi: 36100.00
   POI: "SigXsecOverSM"
   ReadFrom: HIST
   HistoPath: "/nfs/at3/scratch2/orlando/fcnc/output/VLQAnalysisOutputs_new_dis_feb_run_1/"
   DebugLevel: 0
   SystControlPlots: FALSE
   SystPruningShape: 0.01
   SystPruningNorm: 0.01
   CorrelationThreshold: 0.40
   HistoChecks: NOCRASH
   SplitHistoFiles: TRUE
   MCstatThreshold: 0.05
   StatOnly: FALSE
   %BlindingThreshold: 0.05


% -------------------------- %
% -------    FIT     ------- %
% -------------------------- %

Fit: "BONLY"
   FitType: BONLY
   FitRegion: CRSR
   FitBlind: FALSE
   POIAsimov: 0
   SetRandomInitialNPval: 0.9  

% --------------------------- %
% ---------- LIMIT ---------- %
% --------------------------- %

Limit: "limit"
  LimitType: ASYMPTOTIC
  LimitBlind: FALSE
  POIAsimov: 0

% -------------------------- %
% --------- REGIONS -------- %
% -------------------------- %

Region: "c1lep4jex2bex"
Type: SIGNAL
HistoName: "c1lep4jex2bex_FcncDiscriminant"
VariableTitle: "LH discriminant"
Binning: 0.6,1.0
Label: "4jex-2bex"
ShortLabel: "4jex-2bex"

Region: "c1lep4jex3bex"
Type: SIGNAL
HistoName: "c1lep4jex3bex_FcncDiscriminant"
VariableTitle: "LH discriminant"
Binning: 0.0,0.1,0.2,0.3,0.4,0.5,0.6,0.7,0.8
Label: "4jex-3bex"
ShortLabel: "4jex-3bex"

Region: "c1lep4jex4bin"
Type: SIGNAL
HistoName: "c1lep4jex4bin_FcncDiscriminant"
VariableTitle: "LH discriminant"
Binning: 0.0,0.5
Label: "4jex-4bin"
ShortLabel: "4jex-4bin"

Region: "c1lep5jex2bex"
Type: SIGNAL
HistoName: "c1lep5jex2bex_FcncDiscriminant"
VariableTitle: "LH discriminant"
Binning: 0.6,1.0
Label: "5jex-2bex"
ShortLabel: "5jex-2bex"

Region: "c1lep5jex3bex"
Type: SIGNAL
HistoName: "c1lep5jex3bex_FcncDiscriminant"
VariableTitle: "LH discriminant"
Binning: 0.0,0.1,0.2,0.3,0.4,0.5,0.6,0.7,0.8
Label: "5jex-3bex"
ShortLabel: "5jex-3bex"

Region: "c1lep5jex4bin"
Type: SIGNAL
HistoName: "c1lep5jex4bin_FcncDiscriminant"
VariableTitle: "LH discriminant"
Binning: 0.0,0.5
Label: "5jex-4bin"
ShortLabel: "5jex-4bin"

Region: "c1lep6jin2bex"
Type: SIGNAL
HistoName: "c1lep6jin2bex_FcncDiscriminant"
VariableTitle: "LH discriminant"
Binning: 0.6,1.0
Label: "6jin-2bex"
ShortLabel: "6jin-2bex"

Region: "c1lep6jin3bex"
Type: SIGNAL
HistoName: "c1lep6jin3bex_FcncDiscriminant"
VariableTitle: "LH discriminant"
Binning: 0.0,0.1,0.2,0.3,0.4,0.5,0.6,0.7,0.8,0.9
Label: "6jin-3bex"
ShortLabel: "6jin-3bex"

Region: "c1lep6jin4bin"
Type: SIGNAL
HistoName: "c1lep6jin4bin_FcncDiscriminant"
VariableTitle: "LH discriminant"
Binning: 0.0,0.5,1.0
Label: "6jin-4bin"
ShortLabel: "6jin-4bin"

% --------------------------- %
% --------  SAMPLES  -------- %
% --------------------------- %

Sample: "ttbarbb_ref_nom"
  Type: GHOST
  HistoFile: "ttbarbb_notriggermatch"

Sample: "ttbarbb_ref_5f"
  Type: GHOST
  HistoFile: "ttbarbb_fs_notriggermatch"

Sample: "ttbarcc_ref_nom"
  Type: GHOST
  HistoFile: "ttbarcc_notriggermatch"

Sample: "ttbarlight_ref_nom"
  Type: GHOST
  HistoFile: "ttbarlight_notriggermatch"

Systematic: "ttbarbb_generator"
  Title: "t#bar{t}+#geq 1 b generator"
  Type: HISTO
  Samples: ttbarbb
  HistoFileSufUp: "Sherpa5F_notriggermatch"
  Smoothing: 40
  ReferenceSample: ttbarbb_ref_nom
  Symmetrisation: ONESIDED
  Category: t#bar{t} modeling

Systematic: "ttbarbb_PS"
  Title: "t#bar{t}+#geq 1 b PS & Had"
  Type: HISTO
  Samples: ttbarbb
  HistoFileSufUp: "PShad_notriggermatch"
  Smoothing: 40
  ReferenceSample: ttbarbb_ref_nom
  Symmetrisation: ONESIDED
  Category: t#bar{t} modeling

Systematic: "ttbarbb_isr"
  Title: "t#bar{t}+#geq 1 b ISR/FSR"
  Type: HISTO
  Samples: ttbarbb
  HistoFileSufUp: "IsrFsrUP_notriggermatch"
  HistoFileSufDown: "IsrFsrDown_notriggermatch"
  Smoothing: 40
  ReferenceSample: ttbarbb_ref_nom
  Symmetrisation: TWOSIDED
  Category: t#bar{t} modeling


Systematic: "ttbarbb_flavour"
  Title: "t#bar{t}+#geq 1 b 5F vs 4F"
  Type: HISTO
  Samples: ttbarbb
  HistoFileSufUp: "tt1bSherpa4F_notriggermatch"
  %NtupleFileSufDown: ""
  Smoothing: 40
  ReferenceSample: ttbarbb_ref_5f
  Symmetrisation: ONESIDED
  Category: t#bar{t} modeling


Systematic: "ttbarcc_generator"
  Title: "t#bar{t}+#geq 1 c generator"
  Type: HISTO
  Samples: ttbarcc
  HistoFileSufUp: "Sherpa5F_notriggermatch"
  Smoothing: 40
  ReferenceSample: ttbarcc_ref_nom
  Symmetrisation: ONESIDED
  Category: t#bar{t} modeling

Systematic: "ttbarcc_PS"
  Title: "t#bar{t}+#geq 1 c PS & Had"
  Type: HISTO
  Samples: ttbarcc
  HistoFileSufUp: "PShad_notriggermatch"
  Smoothing: 40
  ReferenceSample: ttbarcc_ref_nom
  Symmetrisation: ONESIDED
  Category: t#bar{t} modeling

Systematic: "ttbarcc_isr"
  Title: "t#bar{t}+#geq 1 c ISR/FSR"
  Type: HISTO
  Samples: ttbarcc
  HistoFileSufUp: "IsrFsrUP_notriggermatch"
  HistoFileSufDown: "IsrFsrDown_notriggermatch"
  Smoothing: 40
  ReferenceSample: ttbarcc_ref_nom
  Symmetrisation: TWOSIDED
  Category: t#bar{t} modeling


Systematic: "ttbarlight_generator"
  Title: "t#bar{t}+light generator"
  Type: HISTO
  Samples: ttbarlight
  HistoFileSufUp: "Sherpa5F_notriggermatch"
  Smoothing: 40
  ReferenceSample: ttbarlight_ref_nom
  Symmetrisation: ONESIDED
  Category: t#bar{t} modeling

Systematic: "ttbarlight_PS"
  Title: "t#bar{t}+light PS & Had"
  Type: HISTO
  Samples: ttbarlight
  HistoFileSufUp: "PShad_notriggermatch"
  Smoothing: 40
  ReferenceSample: ttbarlight_ref_nom
  Symmetrisation: ONESIDED
  Category: t#bar{t} modeling

Systematic: "ttbarlight_isr"
  Title: "t#bar{t}+light ISR/FSR"
  Type: HISTO
  Samples: ttbarlight
  HistoFileSufUp: "IsrFsrUP_notriggermatch"
  HistoFileSufDown: "IsrFsrDown_notriggermatch"
  Smoothing: 40
  ReferenceSample: ttbarlight_ref_nom
  Symmetrisation: TWOSIDED
  Category: t#bar{t} modeling



Sample: "Data"
Title: "Data"
Type: data
HistoFile: "Data_data.DAOD_TOPQ1"

Sample: "ttbarlight"
Title: "t#bar{t}+light"
Type: background
FillColor: 0
LineColor: 1
HistoFile: "ttbarlight"

Sample: "ttbarcc"
Title: "t#bar{t}+cc"
Type: background
FillColor: 590
LineColor: 1
HistoFile: "ttbarcc"

Sample: "ttbarbb"
Title: "t#bar{t}+bb"
Type: background
FillColor: 594
LineColor: 1
NormFactor: "ttbbNorm,1,-10,10"
HistoFile: "ttbarbb"

Sample: "topEW"
Title: "topEW"
Type: background
FillColor: 410
LineColor: 1
HistoFile: "topEW"
Group: "Non-t#bar{t}"

Sample: "ttH"
Title: "ttH"
Type: background
FillColor: 410
LineColor: 1
HistoFile: "ttH"
Group: "Non-t#bar{t}"

Sample: "Wjetslight"
Title: "W+jetslight"
Type: background
FillColor: 410
LineColor: 1
HistoFile: "Wjets22light"
Group: "Non-t#bar{t}"
Smooth: TRUE

Sample: "Wjetscharm"
Title: "W+jetscharm"
Type: background
FillColor: 410
LineColor: 1
HistoFile: "Wjets22charm"
Group: "Non-t#bar{t}"
Smooth: TRUE

Sample: "Wjetsbeauty"
Title: "W+jetsbeauty"
Type: background
FillColor: 410
LineColor: 1
HistoFile: "Wjets22beauty"
Group: "Non-t#bar{t}"

Sample: "Zjetslight"
Title: "Z+jetslight"
Type: background
FillColor: 410
LineColor: 1
HistoFile: "Zjets22light"
Group: "Non-t#bar{t}"

Sample: "Zjetscharm"
Title: "Z+jetscharm"
Type: background
FillColor: 410
LineColor: 1
HistoFile: "Zjets22charm"
Group: "Non-t#bar{t}"

Sample: "Zjetsbeauty"
Title: "Z+jetsbeauty"
Type: background
FillColor: 410
LineColor: 1
HistoFile: "Zjets22beauty"
Group: "Non-t#bar{t}"
Smooth: TRUE

Sample: "Singletop"
Title: "Single-top"
Type: background
FillColor: 410
LineColor: 1
HistoFile: "Singletop"
Group: "Non-t#bar{t}"

Sample: "Dibosons"
Title: "Dibosons"
Type: background
FillColor: 410
LineColor: 1
HistoFile: "Dibosons"
Group: "Non-t#bar{t}"


Sample: "SM4tops"
Title: "SM-4tops"
Type: background
FillColor: 410
LineColor: 1
HistoFile: "SM4tops"
Group: "Non-t#bar{t}"


Sample: "QCDE"
Title: "QCDE"
Type: background
FillColor: 410
LineColor: 1
HistoFile: "QCDE"
Group: "Non-t#bar{t}"
NormalizedByTheory: FALSE
Smooth: TRUE

Sample: "QCDMU"
Title: "QCDMU"
Type: background
FillColor: 410
LineColor: 1
HistoFile: "QCDMU"
Group: "Non-t#bar{t}"
NormalizedByTheory: FALSE
Smooth: TRUE


%%%%%%%%%%%%%%%%%%%
%Systematics
%%%%%%%%%%%%%%%%%%%

Systematic: "Lumi"
  Title: "Luminosity"
  Type: OVERALL
  Category: Others
  OverallUp: 0.022
  OverallDown: -0.022
  Exclude: QCDMU,QCDE

Systematic: "ttbar_XS"
  Title: "t#bar{t} inclusive cross section"
  Type: OVERALL
  Category: "t#bar{t} uncertainties"
  OverallUp: 0.055
  OverallDown: -0.061
  Samples: ttbarlight,ttbarbb,ttbarcc,uHcW

Systematic: "ttbarcc_XS"
  Title: "t#bar{t}+c#bar{c} norm."
  Type: OVERALL
  Category: "t#bar{t} uncertainties"
  OverallUp: 0.5
  OverallDown: -0.5
  Samples: ttbarcc

Systematic: "QCDMU_4j_2b"
  Title: "QCDMU norm. 4j2b"
  Type: OVERALL
  Category: "Others"
  OverallUp: 0.5
  OverallDown: -0.5
  Samples: QCDMU
  Regions: c1lep4jex2bex

Systematic: "QCDMU_4j_3b"
  Title: "QCDMU norm. 4j3b"
  Type: OVERALL
  Category: "Others"
  OverallUp: 0.5
  OverallDown: -0.5
  Samples: QCDMU
  Regions: c1lep4jex3bex

Systematic: "QCDMU_4j_4b"
  Title: "QCDMU norm. 4j4b"
  Type: OVERALL
  Category: "Others"
  OverallUp: 0.5
  OverallDown: -0.5
  Samples: QCDMU
  Regions: c1lep4jex4bin

Systematic: "QCDMU_5j_2b"
  Title: "QCDMU norm. 5j2b"
  Type: OVERALL
  Category: "Others"
  OverallUp: 0.5
  OverallDown: -0.5
  Samples: QCDMU
  Regions: c1lep5jex2bex

Systematic: "QCDMU_5j_3b"
  Title: "QCDMU norm. 5j3b"
  Type: OVERALL
  Category: "Others"
  OverallUp: 0.5
  OverallDown: -0.5
  Samples: QCDMU
  Regions: c1lep5jex3bex

Systematic: "QCDMU_5j_4b"
  Title: "QCDMU norm. 5j4b"
  Type: OVERALL
  Category: "Others"
  OverallUp: 0.5
  OverallDown: -0.5
  Samples: QCDMU
  Regions: c1lep5jex4bin

Systematic: "QCDMU_6j_2b"
  Title: "QCDMU norm. 6j2b"
  Type: OVERALL
  Category: "Others"
  OverallUp: 0.5
  OverallDown: -0.5
  Samples: QCDMU
  Regions: c1lep6jin2bex

Systematic: "QCDMU_6j_3b"
  Title: "QCDMU norm. 6j3b"
  Type: OVERALL
  Category: "Others"
  OverallUp: 0.5
  OverallDown: -0.5
  Samples: QCDMU
  Regions: c1lep6jin3bex

Systematic: "QCDMU_6j_4b"
  Title: "QCDMU norm. 6j4b"
  Type: OVERALL
  Category: "Others"
  OverallUp: 0.5
  OverallDown: -0.5
  Samples: QCDMU
  Regions: c1lep6jin4bin

Systematic: "QCDE_4j_2b"
  Title: "QCDE norm. 4j2b"
  Type: OVERALL
  Category: "Others"
  OverallUp: 0.5
  OverallDown: -0.5
  Samples: QCDE
  Regions: c1lep4jex2bex

Systematic: "QCDE_4j_3b"
  Title: "QCDE norm. 4j3b"
  Type: OVERALL
  Category: "Others"
  OverallUp: 0.5
  OverallDown: -0.5
  Samples: QCDE
  Regions: c1lep4jex3bex

Systematic: "QCDE_4j_4b"
  Title: "QCDE norm. 4j4b"
  Type: OVERALL
  Category: "Others"
  OverallUp: 0.5
  OverallDown: -0.5
  Samples: QCDE
  Regions: c1lep4jex4bin

Systematic: "QCDE_5j_2b"
  Title: "QCDE norm. 5j2b"
  Type: OVERALL
  Category: "Others"
  OverallUp: 0.5
  OverallDown: -0.5
  Samples: QCDE
  Regions: c1lep5jex2bex

Systematic: "QCDE_5j_3b"
  Title: "QCDE norm. 5j3b"
  Type: OVERALL
  Category: "Others"
  OverallUp: 0.5
  OverallDown: -0.5
  Samples: QCDE
  Regions: c1lep5jex3bex

Systematic: "QCDE_5j_4b"
  Title: "QCDE norm. 5j4b"
  Type: OVERALL
  Category: "Others"
  OverallUp: 0.5
  OverallDown: -0.5
  Samples: QCDE
  Regions: c1lep5jex4bin

Systematic: "QCDE_6j_2b"
  Title: "QCDE norm. 6j2b"
  Type: OVERALL
  Category: "Others"
  OverallUp: 0.5
  OverallDown: -0.5
  Samples: QCDE
  Regions: c1lep6jin2bex

Systematic: "QCDE_6j_3b"
  Title: "QCDE norm. 6j3b"
  Type: OVERALL
  Category: "Others"
  OverallUp: 0.5
  OverallDown: -0.5
  Samples: QCDE
  Regions: c1lep6jin3bex

Systematic: "QCDE_6j_4b"
  Title: "QCDE norm. 6j4b"
  Type: OVERALL
  Category: "Others"
  OverallUp: 0.5
  OverallDown: -0.5
  Samples: QCDE
  Regions: c1lep6jin4bin

Systematic: "Vjets_XS"
  Title: "V+jets norm."
  Type: OVERALL
  Category: "Background uncertainties"
  OverallUp: 0.30
  OverallDown: -0.30
  Samples: Wjetslight,Zjetslight,Wjetscharm,Zjetscharm,Wjetsbeauty,Zjetsbeauty

Systematic: "Vjets_charmXS"
  Title: "V+cjets norm."
  Type: OVERALL
  Category: "Background uncertainties"
  OverallUp: 0.30
  OverallDown: -0.30
  Samples: Wjetscharm,Zjetscharm

Systematic: "Vjets_beautyXS"
  Title: "V+bjets norm."
  Type: OVERALL
  Category: "Background uncertainties"
  OverallUp: 0.30
  OverallDown: -0.30
  Samples: Wjetsbeauty,Zjetsbeauty

%
% Single top systematics
%

Systematic: "singletop_XS"
  Title: "Single-top norm."
  Type: OVERALL
  Category: "Background uncertainties"
  OverallUp: 0.24
  OverallDown: -0.24
  Samples: Singletop


%
% Dibosons
%

Systematic: "Dibosons_XS"
  Title: "Dibosons norm."
  Type: OVERALL
  Category: "Background uncertainties"
  OverallUp: 0.48
  OverallDown: -0.48
  Samples: Dibosons

%
% TopEW
%

Systematic: "topEW_XS"
  Title: "topEW norm."
  Type: OVERALL
  Category: "Background uncertainties"
  OverallUp: 0.15
  OverallDown: -0.15
  Samples: topEW
  Regions: c1l*

%
% ttH
%

Systematic: "ttH_XS"
  Title: "ttH norm."
  Type: OVERALL
  Category: "Background uncertainties"
  OverallUp: 0.09
  OverallDown: -0.12
  Samples: ttH
  Regions: c1l*

%
% 4tops
%

Systematic: "SM4tops_XS"
  Title: "SM 4tops norm."
  Type: OVERALL
  Category: "Background uncertainties"
  OverallUp: 0.3
  OverallDown: -0.3
  Samples: SM4tops
  Regions: c1l*

% --------------------------- %
% --------   WEIGHT  -------- %
% --------------------------- %

Systematic: "ELEC_ID"
  Title: "Electron ID"
  Type: HISTO
  Category: "Lepton uncertainties"
  HistoNameSufUp: _weight_elec_ID_UP
  HistoNameSufDown: _weight_elec_ID_DOWN
  Symmetrisation: "TwoSided"
  Regions: c1l*
  Exclude: QCDMU,QCDE

Systematic: "ELEC_ISO"
  Title: "Electron isol."
  Type: HISTO
  Category: "Lepton uncertainties"
  HistoNameSufUp: _weight_elec_Iso_UP
  HistoNameSufDown: _weight_elec_Iso_DOWN
  Symmetrisation: "TwoSided"
  Regions: c1l*
  Exclude: QCDMU,QCDE

Systematic: "ELEC_RECO"
  Title: "Electron reco."
  Type: HISTO
  Category: "Lepton uncertainties"
  HistoNameSufUp: _weight_elec_Reco_UP
  HistoNameSufDown: _weight_elec_Reco_DOWN
  Symmetrisation: "TwoSided"
  Regions: c1l*
  Exclude: QCDMU,QCDE

Systematic: "ELEC_TRIGGER_EFF"
  Title: "Electron trig. eff"
  Type: HISTO
  Category: "Lepton uncertainties"
  HistoNameSufUp: _weight_elec_TriggerEff_UP
  HistoNameSufDown: _weight_elec_TriggerEff_DOWN
  Symmetrisation: "TwoSided"
  Regions: c1l*
  Exclude: QCDMU,QCDE

Systematic: "MUON_ID_STAT"
  Title: "Muon ID (stat.)"
  Type: HISTO
  Category: "Lepton uncertainties"
  HistoNameSufUp: _weight_muon_EFF_STAT_UP
  HistoNameSufDown: _weight_muon_EFF_STAT_DOWN
  Symmetrisation: "TwoSided"
  Regions: c1l*
  Exclude: QCDMU,QCDE

Systematic: "MUON_ID_SYS"
  Title: "Muon ID (syst.)"
  Type: HISTO
  Category: "Lepton uncertainties"
  HistoNameSufUp: _weight_muon_EFF_SYS_UP
  HistoNameSufDown: _weight_muon_EFF_SYS_DOWN
  Symmetrisation: "TwoSided"
  Regions: c1l*
  Exclude: QCDMU,QCDE

Systematic: "MUON_TRIG_STAT"
  Title: "Muon Trig. (stat.)"
  Type: HISTO
  Category: "Lepton uncertainties"
  HistoNameSufUp: _weight_muon_TrigStatUncertainty_UP
  HistoNameSufDown: _weight_muon_TrigStatUncertainty_DOWN
  Symmetrisation: "TwoSided"
  Regions: c1l*
  Exclude: QCDMU,QCDE

Systematic: "MUON_TRIG_SYS"
  Title: "Muon Trig. (syst.)"
  Type: HISTO
  Category: "Lepton uncertainties"
  HistoNameSufUp: _weight_muon_TrigSystUncertainty_UP
  HistoNameSufDown: _weight_muon_TrigSystUncertainty_DOWN
  Symmetrisation: "TwoSided"
  Regions: c1l*
  Exclude: QCDMU,QCDE

Systematic: "MUON_ISO_STAT"
  Title: "Muon Isol. (stat.)"
  Type: HISTO
  Category: "Lepton uncertainties"
  HistoNameSufUp: _weight_muon_ISO_STAT_UP
  HistoNameSufDown: _weight_muon_ISO_STAT_DOWN
  Symmetrisation: "TwoSided"
  Regions: c1l*
  Exclude: QCDMU,QCDE

Systematic: "MUON_ISO_SYS"
  Title: "Muon Isol. (syst.)"
  Type: HISTO
  Category: "Lepton uncertainties"
  HistoNameSufUp: _weight_muon_ISO_SYS_UP
  HistoNameSufDown: _weight_muon_ISO_SYS_DOWN
  Symmetrisation: "TwoSided"
  Regions: c1l*
  Exclude: QCDMU,QCDE

Systematic: "MUON_TTVA_STAT"
  Title: "Muon TTVA (stat.)"
  Type: HISTO
  Category: "Lepton uncertainties"
  HistoNameSufUp: _weight_muon_TTVA_STAT_UP
  HistoNameSufDown: _weight_muon_TTVA_STAT_DOWN
  Symmetrisation: "TwoSided"
  Regions: c1l*
  Exclude: QCDMU,QCDE

Systematic: "MUON_TTVA_SYST"
  Title: "Muon TTVA (syst.)"
  Type: HISTO
  Category: "Lepton uncertainties"
  HistoNameSufUp: _weight_muon_TTVA_SYS_UP
  HistoNameSufDown: _weight_muon_TTVA_SYS_DOWN
  Symmetrisation: "TwoSided"
  Regions: c1l*
  Exclude: QCDMU,QCDE

Systematic: "PILEUP_SYST"
  Title: "Pile-up uncertainty"
  Type: HISTO
  Category: "Others"
  HistoNameSufUp: _weight_pu_UP
  HistoNameSufDown: _weight_pu_DOWN
  Symmetrisation: "TwoSided"
%  Exclude: QCD
  Exclude: QCDMU,QCDE


Systematic: "btag_B_EV_0"
  Title: "btag B EV 0"
  Type: HISTO
  Category: "b-tagging uncertainties"
  HistoNameSufUp: _weight_btag_B_EV_Up_0
  HistoNameSufDown:_weight_btag_B_EV_Down_0
  Symmetrisation: "TwoSided"
  Exclude: QCDMU,QCDE

Systematic: "btag_B_EV_1"
  Title: "btag B EV 1"
  Type: HISTO
  Category: "b-tagging uncertainties"
  HistoNameSufUp: _weight_btag_B_EV_Up_1
  HistoNameSufDown:_weight_btag_B_EV_Down_1
  Symmetrisation: "TwoSided"
  Exclude: QCDMU,QCDE

Systematic: "btag_B_EV_2"
  Title: "btag B EV 2"
  Type: HISTO
  Category: "b-tagging uncertainties"
  HistoNameSufUp: _weight_btag_B_EV_Up_2
  HistoNameSufDown:_weight_btag_B_EV_Down_2
  Symmetrisation: "TwoSided"
  Exclude: QCDMU,QCDE

Systematic: "btag_B_EV_3"
  Title: "btag B EV 3"
  Type: HISTO
  Category: "b-tagging uncertainties"
  HistoNameSufUp: _weight_btag_B_EV_Up_3
  HistoNameSufDown:_weight_btag_B_EV_Down_3
  Symmetrisation: "TwoSided"
  Exclude: QCDMU,QCDE

Systematic: "btag_B_EV_4"
  Title: "btag B EV 4"
  Type: HISTO
  Category: "b-tagging uncertainties"
  HistoNameSufUp: _weight_btag_B_EV_Up_4
  HistoNameSufDown:_weight_btag_B_EV_Down_4
  Symmetrisation: "TwoSided"
  Exclude: QCDMU,QCDE

Systematic: "btag_B_EV_5"
  Title: "btag B EV 5"
  Type: HISTO
  Category: "b-tagging uncertainties"
  HistoNameSufUp: _weight_btag_B_EV_Up_5
  HistoNameSufDown:_weight_btag_B_EV_Down_5
  Symmetrisation: "TwoSided"
  Exclude: QCDMU,QCDE

Systematic: "btag_C_EV_0"
  Title: "btag C EV 0"
  Type: HISTO
  Category: "b-tagging uncertainties"
  HistoNameSufUp: _weight_btag_C_EV_Up_0
  HistoNameSufDown:_weight_btag_C_EV_Down_0
  Symmetrisation: "TwoSided"
  Exclude: QCDMU,QCDE

Systematic: "btag_C_EV_1"
  Title: "btag C EV 1"
  Type: HISTO
  Category: "b-tagging uncertainties"
  HistoNameSufUp: _weight_btag_C_EV_Up_1
  HistoNameSufDown:_weight_btag_C_EV_Down_1
  Symmetrisation: "TwoSided"
  Exclude: QCDMU,QCDE

Systematic: "btag_C_EV_2"
  Title: "btag C EV 2"
  Type: HISTO
  Category: "b-tagging uncertainties"
  HistoNameSufUp: _weight_btag_C_EV_Up_2
  HistoNameSufDown:_weight_btag_C_EV_Down_2
  Symmetrisation: "TwoSided"
  Exclude: QCDMU,QCDE

Systematic: "btag_C_EV_3"
  Title: "btag C EV 3"
  Type: HISTO
  Category: "b-tagging uncertainties"
  HistoNameSufUp: _weight_btag_C_EV_Up_3
  HistoNameSufDown:_weight_btag_C_EV_Down_3
  Symmetrisation: "TwoSided"
  Exclude: QCDMU,QCDE

Systematic: "btag_L_EV_0"
  Title: "btag L EV 0"
  Type: HISTO
  Category: "b-tagging uncertainties"
  HistoNameSufUp: _weight_btag_Light_EV_Up_0
  HistoNameSufDown:_weight_btag_Light_EV_Down_0
  Symmetrisation: "TwoSided"
  Exclude: QCDMU,QCDE

Systematic: "btag_L_EV_1"
  Title: "btag L EV 1"
  Type: HISTO
  Category: "b-tagging uncertainties"
  HistoNameSufUp: _weight_btag_Light_EV_Up_1
  HistoNameSufDown:_weight_btag_Light_EV_Down_1
  Symmetrisation: "TwoSided"
  Exclude: QCDMU,QCDE

Systematic: "btag_L_EV_2"
  Title: "btag L EV 2"
  Type: HISTO
  Category: "b-tagging uncertainties"
  HistoNameSufUp: _weight_btag_Light_EV_Up_2
  HistoNameSufDown:_weight_btag_Light_EV_Down_2
  Symmetrisation: "TwoSided"
  Exclude: QCDMU,QCDE

Systematic: "btag_L_EV_3"
  Title: "btag L EV 3"
  Type: HISTO
  Category: "b-tagging uncertainties"
  HistoNameSufUp: _weight_btag_Light_EV_Up_3
  HistoNameSufDown:_weight_btag_Light_EV_Down_3
  Symmetrisation: "TwoSided"
  Exclude: QCDMU,QCDE

Systematic: "btag_L_EV_4"
  Title: "btag L EV 4"
  Type: HISTO
  Category: "b-tagging uncertainties"
  HistoNameSufUp: _weight_btag_Light_EV_Up_4
  HistoNameSufDown:_weight_btag_Light_EV_Down_4
  Symmetrisation: "TwoSided"
  Exclude: QCDMU,QCDE

Systematic: "btag_L_EV_5"
  Title: "btag L EV 5"
  Type: HISTO
  Category: "b-tagging uncertainties"
  HistoNameSufUp: _weight_btag_Light_EV_Up_5
  HistoNameSufDown:_weight_btag_Light_EV_Down_5
  Symmetrisation: "TwoSided"
  Exclude: QCDMU,QCDE

Systematic: "btag_L_EV_6"
  Title: "btag L EV 6"
  Type: HISTO
  Category: "b-tagging uncertainties"
  HistoNameSufUp: _weight_btag_Light_EV_Up_6
  HistoNameSufDown:_weight_btag_Light_EV_Down_6
  Symmetrisation: "TwoSided"
  Exclude: QCDMU,QCDE

Systematic: "btag_L_EV_7"
  Title: "btag L EV 7"
  Type: HISTO
  Category: "b-tagging uncertainties"
  HistoNameSufUp: _weight_btag_Light_EV_Up_7
  HistoNameSufDown:_weight_btag_Light_EV_Down_7
  Symmetrisation: "TwoSided"
  Exclude: QCDMU,QCDE

Systematic: "btag_L_EV_8"
  Title: "btag L EV 8"
  Type: HISTO
  Category: "b-tagging uncertainties"
  HistoNameSufUp: _weight_btag_Light_EV_Up_8
  HistoNameSufDown:_weight_btag_Light_EV_Down_8
  Symmetrisation: "TwoSided"
  Exclude: QCDMU,QCDE

Systematic: "btag_L_EV_9"
  Title: "btag L EV 9"
  Type: HISTO
  Category: "b-tagging uncertainties"
  HistoNameSufUp: _weight_btag_Light_EV_Up_9
  HistoNameSufDown:_weight_btag_Light_EV_Down_9
  Symmetrisation: "TwoSided"
  Exclude: QCDMU,QCDE

Systematic: "btag_L_EV_10"
  Title: "btag L EV 10"
  Type: HISTO
  Category: "b-tagging uncertainties"
  HistoNameSufUp: _weight_btag_Light_EV_Up_10
  HistoNameSufDown:_weight_btag_Light_EV_Down_10
  Symmetrisation: "TwoSided"
  Exclude: QCDMU,QCDE

Systematic: "btag_L_EV_11"
  Title: "btag L EV 11"
  Type: HISTO
  Category: "b-tagging uncertainties"
  HistoNameSufUp: _weight_btag_Light_EV_Up_11
  HistoNameSufDown:_weight_btag_Light_EV_Down_11
  Symmetrisation: "TwoSided"
  Exclude: QCDMU,QCDE

Systematic: "btag_L_EV_12"
  Title: "btag L EV 12"
  Type: HISTO
  Category: "b-tagging uncertainties"
  HistoNameSufUp: _weight_btag_Light_EV_Up_12
  HistoNameSufDown:_weight_btag_Light_EV_Down_12
  Symmetrisation: "TwoSided"
  Exclude: QCDMU,QCDE

Systematic: "btag_L_EV_13"
  Title: "btag L EV 13"
  Type: HISTO
  Category: "b-tagging uncertainties"
  HistoNameSufUp: _weight_btag_Light_EV_Up_13
  HistoNameSufDown:_weight_btag_Light_EV_Down_13
  Symmetrisation: "TwoSided"
  Exclude: QCDMU,QCDE

Systematic: "btag_L_EV_14"
  Title: "btag L EV 14"
  Type: HISTO
  Category: "b-tagging uncertainties"
  HistoNameSufUp: _weight_btag_Light_EV_Up_14
  HistoNameSufDown:_weight_btag_Light_EV_Down_14
  Symmetrisation: "TwoSided"
  Exclude: QCDMU,QCDE

Systematic: "btag_L_EV_15"
  Title: "btag L EV 15"
  Type: HISTO
  Category: "b-tagging uncertainties"
  HistoNameSufUp: _weight_btag_Light_EV_Up_15
  HistoNameSufDown:_weight_btag_Light_EV_Down_15
  Symmetrisation: "TwoSided"
  Exclude: QCDMU,QCDE

Systematic: "btag_L_EV_16"
  Title: "btag L EV 16"
  Type: HISTO
  Category: "b-tagging uncertainties"
  HistoNameSufUp: _weight_btag_Light_EV_Up_16
  HistoNameSufDown:_weight_btag_Light_EV_Down_16
  Symmetrisation: "TwoSided"
  Exclude: QCDMU,QCDE

Systematic: "BTAG_EXTRAP"
  Title: "b-tag extrap."
  Type: HISTO
  Category: "b-tagging uncertainties"
  HistoNameSufUp: _weight_btag_extrapolation_Up
  HistoNameSufDown: _weight_btag_extrapolation_Down
  Symmetrisation: "TwoSided"
  Exclude: QCDMU,QCDE

Systematic: "BTAG_EXTRAP_FROM_CHARM"
  Title: "b-tag extrap. from charm"
  Type: HISTO
  Category: "b-tagging uncertainties"
  HistoNameSufUp: _weight_btag_extrapolation_from_charm_Up
  HistoNameSufDown: _weight_btag_extrapolation_from_charm_Down
  Symmetrisation: "TwoSided"
  Exclude: QCDMU,QCDE

Systematic: "JVT_SF"
  Title: "JVT uncertainty"
  Type: HISTO
  Category: "Jet uncertainties"
  HistoNameSufUp: _weight_jvt_UP
  HistoNameSufDown: _weight_jvt_DOWN
  Symmetrisation: "TwoSided"
  Exclude: QCDMU,QCDE

%
% Ttbar+bb systematics
%


Systematic: "TTBB_SCALE"
  Title: "t#bar{t}+b#bar{b} scale"
  Type: HISTO
  Category: "t#bar{t} uncertainties"
  HistoNameSufUp: _weight_ttbar_pp8_norm_rw_SherpaX05
  HistoNameSufDown: _weight_ttbar_pp8_norm_rw_SherpaX2
  Symmetrisation: "TwoSided"
  Samples: ttbarbb

Systematic: "TTBB_CSS_KIN"
  Title: "t#bar{t}+b#bar{b} CSS KIN"
  Type: HISTO
  Category: "t#bar{t} uncertainties"
  HistoNameSufUp: _weight_ttbar_pp8_norm_rw_SherpaCSSKIN
  Symmetrisation: "OneSided"
  Samples: ttbarbb

Systematic: "TTBB_MSTW"
  Title: "t#bar{t}+b#bar{b} MSTW"
  Type: HISTO
  Category: "t#bar{t} uncertainties"
  HistoNameSufUp: _weight_ttbar_pp8_norm_rw_SherpaMSTW
  Symmetrisation: "OneSided"
  Samples: ttbarbb

Systematic: "TTBB_NNPDF"
  Title: "t#bar{t}+b#bar{b} NNPDF"
  Type: HISTO
  Category: "t#bar{t} uncertainties"
  HistoNameSufUp: _weight_ttbar_pp8_norm_rw_SherpaNNPDF
  Symmetrisation: "OneSided"
  Samples: ttbarbb

Systematic: "TTBB_Q_CMMPS"
  Title: "t#bar{t}+b#bar{b} Q CMMPS"
  Type: HISTO
  Category: "t#bar{t} uncertainties"
  HistoNameSufUp: _weight_ttbar_pp8_norm_rw_SherpaQCMMPS
  Symmetrisation: "OneSided"
  Samples: ttbarbb

Systematic: "TTBB_GLOSOFT"
  Title: "t#bar{t}+b#bar{b} glo soft"
  Type: HISTO
  Category: "t#bar{t} uncertainties"
  HistoNameSufUp: _weight_ttbar_pp8_norm_rw_SherpaGlosoft
  Symmetrisation: "OneSided"
  Samples: ttbarbb

Systematic: "TTBB_UE"
  Title: "t#bar{t}+b#bar{b} UE modeling"
  Type: HISTO
  Category: "t#bar{t} uncertainties"
  HistoNameSufUp: _weight_ttbar_pp8_norm_rw_SherpaMPIup
  HistoNameSufDown: _weight_ttbar_pp8_norm_rw_SherpaMPIdown
  Symmetrisation: "TwoSided"
  Samples: ttbarbb


% --------------------------- %
% ---  TTBAR SYSTEMATICS  --- %
% --------------------------- %

%Systematic: "ttbarlight_PS"
%  Title: "t#bar{t}+light parton shower"
%  Type: HISTO
%  Category: "t#bar{t} uncertainties"
%  HistoFileSufUp: PShad
%  Symmetrisation: "OneSided"
%  Smoothing: 40
%  Samples: ttbarlight

%Systematic: "ttbarlight_Sherpa5F"
%  Title: "t#bar{t}+light Sherpa5F"
%  Type: HISTO
%  Category: "t#bar{t} uncertainties"
%  HistoFileSufUp: Sherpa5F
%  Symmetrisation: "OneSided"
%  Smoothing: 40
%  Samples: ttbarlight

%Systematic: "ttbarlight_tt1bSherpa4F"
%  Title: "t#bar{t}+light tt1bSherpa4F"
%  Type: HISTO
%  Category: "t#bar{t} uncertainties"
%  HistoFileSufUp: tt1bSherpa4F
%  Symmetrisation: "OneSided"
%  Smoothing: 40
%  Samples: ttbarlight

%Systematic: "ttbarlight_IsrFsr"
%  Title: "t#bar{t}+light IsrFsr"
%  Type: HISTO
%  Category: "t#bar{t} uncertainties"
%  HistoFileSufUp: IsrFsr
%  Symmetrisation: "OneSided"
%  Smoothing: 40
%  Samples: ttbarlight

%Systematic: "ttbarcc_PS"
%  Title: "t#bar{t}+#geq 1c parton shower"
%  Type: HISTO
%  Category: "t#bar{t} uncertainties"
%  HistoFileSufUp: PShad
%  Symmetrisation: "OneSided"
%  Smoothing: 40
%  Samples: ttbarcc

%Systematic: "ttbarcc_Sherpa5F"
%  Title: "t#bar{t}+#geq 1c Sherpa5F"
%  Type: HISTO
%  Category: "t#bar{t} uncertainties"
%  HistoFileSufUp: Sherpa5F
%  Symmetrisation: "OneSided"
%  Smoothing: 40
%  Samples: ttbarcc

%Systematic: "ttbarcc_IsrFsr"
%  Title: "t#bar{t}+#geq 1c IsrFsr"
%  Type: HISTO
%  Category: "t#bar{t} uncertainties"
%  HistoFileSufUp: IsrFsr
%  Symmetrisation: "OneSided"
%  Smoothing: 40
%  Samples: ttbarcc

%Systematic: "ttbarcc_tt1bSherpa4F"
%  Title: "t#bar{t}+#geq 1c tt1bSherpa4F"
%  Type: HISTO
%  Category: "t#bar{t} uncertainties"
%  HistoFileSufUp: tt1bSherpa4F
%  Symmetrisation: "OneSided"
%  Smoothing: 40
%  Samples: ttbarcc

%Systematic: "ttbarbb_PS"
%  Title: "t#bar{t}+bb parton shower"
%  Type: HISTO
%  Category: "t#bar{t} uncertainties"
%  HistoFileSufUp: PShad
%  Symmetrisation: "OneSided"
%  Smoothing: 40
%  Samples: ttbarbb

%Systematic: "ttbarbb_Sherpa5F"
%  Title: "t#bar{t}+bb Sherpa5F"
%  Type: HISTO
%  Category: "t#bar{t} uncertainties"
%  HistoFileSufUp: Sherpa5F
%  Symmetrisation: "OneSided"
%  Smoothing: 40
%  Samples: ttbarbb

%Systematic: "ttbarbb_tt1bSherpa4F"
%  Title: "t#bar{t}+#geq 1b tt1bSherpa4F"
%  Type: HISTO
%  Category: "t#bar{t} uncertainties"
%  HistoFileSufUp: tt1bSherpa4F
%  Symmetrisation: "OneSided"
%  Smoothing: 40
%  Samples: ttbarbb

%Systematic: "ttbarbb_IsrFsr"
%  Title: "t#bar{t}+bb IsrFsr"
%  Type: HISTO
%  Category: "t#bar{t} uncertainties"
%  HistoFileSufUp: IsrFsr
%  Symmetrisation: "OneSided"
%  Smoothing: 40
%  Samples: ttbarbb

% --------------------------- %
% --------   OBJECT  -------- %
% --------------------------- %

Systematic: "EG_RESOLUTION_ALL"
  Title: "Electron E_{T} res."
  Type: HISTO
  Category: "Lepton uncertainties"
  HistoFileSufUp: _EG_RESOLUTION_ALL__1up
  HistoFileSufDown: _EG_RESOLUTION_ALL__1down
  Symmetrisation: "TwoSided"
  Smoothing: 40
  Exclude: QCDMU,QCDE

Systematic: "EG_SCALE_ALL"
  Title: "Electron E_{T} scale"
  Type: HISTO
  Category: "Lepton uncertainties"
  HistoFileSufUp: _EG_SCALE_ALL__1up
  HistoFileSufDown: _EG_SCALE_ALL__1down
  Symmetrisation: "TwoSided"
  Smoothing: 40
  Exclude: QCDMU,QCDE

Systematic: "MUON_ID"
  Title: "Muon ID p_{T} res."
  Type: HISTO
  Category: "Lepton uncertainties"
  HistoFileSufUp: _MUON_ID__1up
  HistoFileSufDown: _MUON_ID__1down
  Symmetrisation: "TwoSided"
  Smoothing: 40
  Exclude: QCDMU,QCDE

Systematic: "MUON_MS"
  Title: "Muon MS p_{T} res."
  Type: HISTO
  Category: "Lepton uncertainties"
  HistoFileSufUp: _MUON_MS__1up
  HistoFileSufDown: _MUON_MS__1down
  Symmetrisation: "TwoSided"
  Smoothing: 40
  Exclude: QCDMU,QCDE

Systematic: "MUON_SCALE"
  Title: "Muon p_{T} scale"
  Type: HISTO
  Category: "Lepton uncertainties"
  HistoFileSufUp: _MUON_SCALE__1up
  HistoFileSufDown: _MUON_SCALE__1down
  Symmetrisation: "TwoSided"
  Smoothing: 40
  Exclude: QCDMU,QCDE

Systematic: "MUON_SAGITTA_RESBIAS"
  Title: "Muon p_{T} scale"
  Type: HISTO
  Category: "Lepton uncertainties"
  HistoFileSufUp: _MUON_SAGITTA_RESBIAS__1up
  HistoFileSufDown: _MUON_SAGITTA_RESBIAS__1down
  Symmetrisation: "TwoSided"
  Smoothing: 40
  Exclude: QCDMU,QCDE

Systematic: "JET_BJES_Response"
  Title: "b JES response"
  Type: HISTO
  Category: "Jet uncertainties"
  HistoFileSufUp: _JET_BJES_Response__1up
  HistoFileSufDown: _JET_BJES_Response__1down
  Symmetrisation: "TwoSided"
  Smoothing: 40
  Exclude: QCDMU,QCDE

Systematic: "JET_EffectiveNP_1"
  Title: "Effective NP 1"
  Type: HISTO
  Category: "Jet uncertainties"
  HistoFileSufUp: _JET_EffectiveNP_1__1up
  HistoFileSufDown: _JET_EffectiveNP_1__1down
  Symmetrisation: "TwoSided"
  Smoothing: 40
  Exclude: QCDMU,QCDE

Systematic: "JET_EffectiveNP_2"
  Title: "Effective NP 2"
  Type: HISTO
  Category: "Jet uncertainties"
  HistoFileSufUp: _JET_EffectiveNP_2__1up
  HistoFileSufDown: _JET_EffectiveNP_2__1down
  Symmetrisation: "TwoSided"
  Smoothing: 40
  Exclude: QCDMU,QCDE

Systematic: "JET_EffectiveNP_3"
  Title: "Effective NP 3"
  Type: HISTO
  Category: "Jet uncertainties"
  HistoFileSufUp: _JET_EffectiveNP_3__1up
  HistoFileSufDown: _JET_EffectiveNP_3__1down
  Symmetrisation: "TwoSided"
  Smoothing: 40
  Exclude: QCDMU,QCDE

Systematic: "JET_EffectiveNP_4"
  Title: "Effective NP 4"
  Type: HISTO
  Category: "Jet uncertainties"
  HistoFileSufUp: _JET_EffectiveNP_4__1up
  HistoFileSufDown: _JET_EffectiveNP_4__1down
  Symmetrisation: "TwoSided"
  Smoothing: 40
  Exclude: QCDMU,QCDE

Systematic: "JET_EffectiveNP_5"
  Title: "Effective NP 5"
  Type: HISTO
  Category: "Jet uncertainties"
  HistoFileSufUp: _JET_EffectiveNP_5__1up
  HistoFileSufDown: _JET_EffectiveNP_5__1down
  Symmetrisation: "TwoSided"
  Smoothing: 40
  Exclude: QCDMU,QCDE

Systematic: "JET_EffectiveNP_8restTerm"
  Title: "Effective NP 6"
  Type: HISTO
  Category: "Jet uncertainties"
  HistoFileSufUp: _JET_EffectiveNP_8restTerm__1up
  HistoFileSufDown: _JET_EffectiveNP_8restTerm__1down
  Symmetrisation: "TwoSided"
  Smoothing: 40
  Exclude: QCDMU,QCDE

Systematic: "JET_EtaIntercalibration_Modelling"
  Title: "#eta intercalib. (model)"
  Type: HISTO
  Category: "Jet uncertainties"
  HistoFileSufUp: _JET_EtaIntercalibration_Modelling__1up
  HistoFileSufDown: _JET_EtaIntercalibration_Modelling__1down
  Symmetrisation: "TwoSided"
  Smoothing: 40
  Exclude: QCDMU,QCDE

Systematic: "JET_EtaIntercalibration_TotalStat"
  Title: "#eta intercalib. (stat.)"
  Type: HISTO
  Category: "Jet uncertainties"
  HistoFileSufUp: _JET_EtaIntercalibration_TotalStat__1up
  HistoFileSufDown: _JET_EtaIntercalibration_TotalStat__1down
  Symmetrisation: "TwoSided"
  Smoothing: 40
  Exclude: QCDMU,QCDE

Systematic: "JET_EtaIntercalibration_NonClosure"
  Title: "#eta intercalib. (stat.)"
  Type: HISTO
  Category: "Jet uncertainties"
  HistoFileSufUp: _JET_EtaIntercalibration_NonClosure__1up
  HistoFileSufDown: _JET_EtaIntercalibration_NonClosure__1down
  Symmetrisation: "TwoSided"
  Smoothing: 40
  Exclude: QCDMU,QCDE

Systematic: "JET_Flavor_Composition"
  Title: "Flavour composition"
  Type: HISTO
  Category: "Jet uncertainties"
  HistoFileSufUp: _JET_Flavor_Composition__1up
  HistoFileSufDown: _JET_Flavor_Composition__1down
  Symmetrisation: "TwoSided"
  Smoothing: 40
  Exclude: QCDMU,QCDE

Systematic: "JET_Flavor_Response"
  Title: "Flavor response"
  Type: HISTO
  Category: "Jet uncertainties"
  HistoFileSufUp: _JET_Flavor_Response__1up
  HistoFileSufDown: _JET_Flavor_Response__1down
  Symmetrisation: "TwoSided"
  Smoothing: 40
  Exclude: QCDMU,QCDE

Systematic: "JET_Pileup_OffsetMu"
  Title: "Pile-up offset mu term"
  Type: HISTO
  Category: "Jet uncertainties"
  HistoFileSufUp: _JET_Pileup_OffsetMu__1up
  HistoFileSufDown: _JET_Pileup_OffsetMu__1down
  Symmetrisation: "TwoSided"
  Smoothing: 40
  Exclude: QCDMU,QCDE

Systematic: "JET_Pileup_OffsetNPV"
  Title: "Pile-up offset NPV term"
  Type: HISTO
  Category: "Jet uncertainties"
  HistoFileSufUp: _JET_Pileup_OffsetNPV__1up
  HistoFileSufDown: _JET_Pileup_OffsetNPV__1down
  Symmetrisation: "TwoSided"
  Smoothing: 40
  Exclude: QCDMU,QCDE

Systematic: "JET_Pileup_PtTerm"
  Title: "Pile-up offset p_{T} term"
  Type: HISTO
  Category: "Jet uncertainties"
  HistoFileSufUp: _JET_Pileup_PtTerm__1up
  HistoFileSufDown: _JET_Pileup_PtTerm__1down
  Symmetrisation: "TwoSided"
  Smoothing: 40
  Exclude: QCDMU,QCDE

Systematic: "JET_Pileup_RhoTopology"
  Title: "#rho topology"
  Type: HISTO
  Category: "Jet uncertainties"
  HistoFileSufUp: _JET_Pileup_RhoTopology__1up
  HistoFileSufDown: _JET_Pileup_RhoTopology__1down
  Symmetrisation: "TwoSided"
  Smoothing: 40
  Exclude: QCDMU,QCDE

%Systematic: "JET_PunchThrough_MC15"
% Title: "Punch-through correction"
%  Type: HISTO
%  Category: "Jet uncertainties"
%  HistoFileSufUp: _JET_PunchThrough_MC15__1up
%  HistoFileSufDown: _JET_PunchThrough_MC15__1down
%  Symmetrisation: "TwoSided"
%  Smoothing: 40
%  Exclude: uHbWp 

Systematic: "JET_SingleParticle_HighPt"
  Title: "High-p_{T} term"
  Type: HISTO
  Category: "Jet uncertainties"
  HistoFileSufUp: _JET_SingleParticle_HighPt__1up
  HistoFileSufDown: _JET_SingleParticle_HighPt__1down
  Symmetrisation: "TwoSided"
  Smoothing: 40
  Exclude: QCDMU,QCDE


Systematic: "JET_JER_SINGLE_NP__1up"
  Title: "JER"
  Type: HISTO
  Category: "Jet uncertainties"
  HistoFileSufUp: _JET_JER_SINGLE_NP__1up
  Symmetrisation: "OneSided"
  Smoothing: 40
  Exclude: QCDMU,QCDE

Systematic: "MET_SoftTrk_ResoPara"
  Title: "MET TST reso. para."
  Type: HISTO
  Category: "E_{T}^{miss} uncertainties"
  HistoFileSufUp: _MET_SoftTrk_ResoPara
  Symmetrisation: "OneSided"
  Smoothing: 40
  Exclude: QCDMU,QCDE

Systematic: "MET_SoftTrk_ResoPerp"
  Title: "MET TST reso. perp."
  Type: HISTO
  Category: "E_{T}^{miss} uncertainties"
  HistoFileSufUp: _MET_SoftTrk_ResoPerp
  Symmetrisation: "OneSided"
  Smoothing: 40
  Exclude: QCDMU,QCDE

Systematic: "MET_SoftTrk_Scale"
  Title: "MET TST scale"
  Type: HISTO
  Category: "E_{T}^{miss} uncertainties"
  HistoFileSufUp: _MET_SoftTrk_ScaleUp
  HistoFileSufDown: _MET_SoftTrk_ScaleDown
  Symmetrisation: "TwoSided"
  Smoothing: 40
  Exclude: QCDMU,QCDE

% --------------------------- %
% --------- SIGNALS --------- %
% --------------------------- %
 
Sample: "uHcW"
  Title: "t#rightarrow Hc"
  Type: "signal"
  NormFactor: "SigXsecOverSM",1.,0,100
  %NormFactor: "SigXsecOverSM",1.,0,100
  FillColor: 2
  LineColor: 2
  HistoFile: uHcW
 



 
 
