% -------------------------- %
% -------    JOB     ------- %
% -------------------------- %

Job: "base_ebv2_signal_uH_default_stat_only"
   Label: "FCNC analysis"
   CmeLabel: "13 TeV"
   LumiLabel: "36.1 fb^{-1}"
   Lumi: 36100.00
   POI: "SigXsecOverSM"
   ReadFrom: HIST
   HistoPath: "/nfs/at3/scratch2/orlando/fcnc/output/VLQAnalysisOutputs_new_dis_feb_run_1/"
   DebugLevel: 0
   SystControlPlots: FALSE
   SystPruningShape: 0.01
   SystPruningNorm: 0.01
   CorrelationThreshold: 0.40
   HistoChecks: NOCRASH
   SplitHistoFiles: TRUE
   MCstatThreshold: 0.05
   StatOnly: TRUE
   %BlindingThreshold: 0.05


% -------------------------- %
% -------    FIT     ------- %
% -------------------------- %

Fit: "BONLY"
   FitType: BONLY
   FitRegion: CRSR
   FitBlind: FALSE
   POIAsimov: 0
   SetRandomInitialNPval: 0.5  

% --------------------------- %
% ---------- LIMIT ---------- %
% --------------------------- %

Limit: "limit"
  LimitType: ASYMPTOTIC
  LimitBlind: FALSE
  POIAsimov: 0

% -------------------------- %
% --------- REGIONS -------- %
% -------------------------- %

Region: "c1lep4jex2bex"
Type: SIGNAL
HistoName: "c1lep4jex2bex_FcncDiscriminant"
VariableTitle: "LH discriminant"
Binning: 0.6,1.0
Label: "4jex-2bex"
ShortLabel: "4jex-2bex"

Region: "c1lep4jex3bex"
Type: SIGNAL
HistoName: "c1lep4jex3bex_FcncDiscriminant"
VariableTitle: "LH discriminant"
Binning: 0.0,0.1,0.2,0.3,0.4,0.5,0.6,0.7,0.8,0.9,1.0
Label: "4jex-3bex"
ShortLabel: "4jex-3bex"

Region: "c1lep4jex4bin"
Type: SIGNAL
HistoName: "c1lep4jex4bin_FcncDiscriminant"
VariableTitle: "LH discriminant"
Binning: 0.0,0.5,1.0
Label: "4jex-4bin"
ShortLabel: "4jex-4bin"

Region: "c1lep5jex2bex"
Type: SIGNAL
HistoName: "c1lep5jex2bex_FcncDiscriminant"
VariableTitle: "LH discriminant"
Binning: 0.6,1.0
Label: "5jex-2bex"
ShortLabel: "5jex-2bex"

Region: "c1lep5jex3bex"
Type: SIGNAL
HistoName: "c1lep5jex3bex_FcncDiscriminant"
VariableTitle: "LH discriminant"
Binning: 0.0,0.1,0.2,0.3,0.4,0.5,0.6,0.7,0.8,0.9,1.0
Label: "5jex-3bex"
ShortLabel: "5jex-3bex"

Region: "c1lep5jex4bin"
Type: SIGNAL
HistoName: "c1lep5jex4bin_FcncDiscriminant"
VariableTitle: "LH discriminant"
Binning: 0.0,0.5,1.0
Label: "5jex-4bin"
ShortLabel: "5jex-4bin"

Region: "c1lep6jin2bex"
Type: SIGNAL
HistoName: "c1lep6jin2bex_FcncDiscriminant"
VariableTitle: "LH discriminant"
Binning: 0.6,1.0
Label: "6jin-2bex"
ShortLabel: "6jin-2bex"

Region: "c1lep6jin3bex"
Type: SIGNAL
HistoName: "c1lep6jin3bex_FcncDiscriminant"
VariableTitle: "LH discriminant"
Binning: 0.0,0.1,0.2,0.3,0.4,0.5,0.6,0.7,0.8,0.9,1.0
Label: "6jin-3bex"
ShortLabel: "6jin-3bex"

Region: "c1lep6jin4bin"
Type: SIGNAL
HistoName: "c1lep6jin4bin_FcncDiscriminant"
VariableTitle: "LH discriminant"
Binning: 0.0,0.5,1.0
Label: "6jin-4bin"
ShortLabel: "6jin-4bin"


%Sample: "Data"
%Title: "Data"
%Type: data
%HistoFile: "Data_data.DAOD_TOPQ1"

Sample: "ttbarlight"
Title: "t#bar{t}+light"
Type: background
FillColor: 0
LineColor: 1
HistoFile: "ttbarlight"

Sample: "ttbarcc"
Title: "t#bar{t}+cc"
Type: background
FillColor: 590
LineColor: 1
HistoFile: "ttbarcc"

Sample: "ttbarbb"
Title: "t#bar{t}+bb"
Type: background
FillColor: 594
LineColor: 1
%NormFactor: "ttbbNorm,1,-10,10"
HistoFile: "ttbarbb"

Sample: "topEW"
Title: "topEW"
Type: background
FillColor: 410
LineColor: 1
HistoFile: "topEW"
Group: "Non-t#bar{t}"

Sample: "ttH"
Title: "ttH"
Type: background
FillColor: 410
LineColor: 1
HistoFile: "ttH"
Group: "Non-t#bar{t}"

Sample: "Wjetslight"
Title: "W+jetslight"
Type: background
FillColor: 410
LineColor: 1
HistoFile: "Wjets22light"
Group: "Non-t#bar{t}"
Smooth: TRUE

Sample: "Wjetscharm"
Title: "W+jetscharm"
Type: background
FillColor: 410
LineColor: 1
HistoFile: "Wjets22charm"
Group: "Non-t#bar{t}"
Smooth: TRUE

Sample: "Wjetsbeauty"
Title: "W+jetsbeauty"
Type: background
FillColor: 410
LineColor: 1
HistoFile: "Wjets22beauty"
Group: "Non-t#bar{t}"

Sample: "Zjetslight"
Title: "Z+jetslight"
Type: background
FillColor: 410
LineColor: 1
HistoFile: "Zjets22light"
Group: "Non-t#bar{t}"
Smooth: TRUE

Sample: "Zjetscharm"
Title: "Z+jetscharm"
Type: background
FillColor: 410
LineColor: 1
HistoFile: "Zjets22charm"
Group: "Non-t#bar{t}"
Smooth: TRUE

Sample: "Zjetsbeauty"
Title: "Z+jetsbeauty"
Type: background
FillColor: 410
LineColor: 1
HistoFile: "Zjets22beauty"
Group: "Non-t#bar{t}"

Sample: "Singletop"
Title: "Single-top"
Type: background
FillColor: 410
LineColor: 1
HistoFile: "Singletop"
Group: "Non-t#bar{t}"

Sample: "Dibosons"
Title: "Dibosons"
Type: background
FillColor: 410
LineColor: 1
HistoFile: "Dibosons"
Group: "Non-t#bar{t}"

Sample: "QCDE"
Title: "QCDE"
Type: background
FillColor: 410
LineColor: 1
HistoFile: "QCDE"
Group: "Non-t#bar{t}"
NormalizedByTheory: FALSE
Smooth: TRUE

Sample: "QCDMU"
Title: "QCDMU"
Type: background
FillColor: 410
LineColor: 1
HistoFile: "QCDMU"
Group: "Non-t#bar{t}"
NormalizedByTheory: FALSE
Smooth: TRUE


Sample: "SM4tops"
Title: "SM-4tops"
Type: background
FillColor: 410
LineColor: 1
HistoFile: "SM4tops"
Group: "Non-t#bar{t}"

% --------------------------- %
% --------- SIGNALS --------- %
% --------------------------- %
 
Sample: "uHbW"
  Title: "t#rightarrow Hu"
  Type: "signal"
  NormFactor: "SigXsecOverSM",1.0,0,100
  %NormFactor: "SigXsecOverSM",1.,0,100
  FillColor: 2
  LineColor: 2
  HistoFile: uHbW
 



 
 
